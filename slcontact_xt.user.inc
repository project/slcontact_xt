<?php
/**
 * @package	SlContact_xt
 * @copyright	Copyright (C) 2009 Wene / ssm2017 Binder (S.Massiaux). All rights reserved.
 * @license		GNU/GPL, http://www.gnu.org/licenses/gpl-2.0.html
 * SlContact_xt is free software. This version may have been modified pursuant to the GNU General Public License,
 * and as distributed it includes or is derivative of works licensed under the GNU General Public License
 * or other free or open source software licenses.
 */

/**
* Implementation of hook_user().
*/
function slcontact_xt_user($op, &$edit, &$account, $category = NULL) {
  $admin = user_access('administer users');
  if ($op == 'form' && $category == 'account') {
    $form['slcontact_xt'] = array(
      '#type'          => 'fieldset',
      '#title'           => t('SlContact_xt settings'),
      '#weight'       => 10,
      '#collapsible'  => TRUE,
    );
    if ($admin) {
      $form['slcontact_xt']['slcontact_xt_expiration_date'] = array(
        '#type'              => 'date',
        '#title'               => t('Expiration date'),
        '#description'     => t('Define the expiration date'),
        '#default_value' => !empty($edit['slcontact_xt_expiration_date']) ? $edit['slcontact_xt_expiration_date'] : slcontact_xt_date2array(),
        '#required'         => FALSE
      );
    }
    $form['slcontact_xt']['slcontact_xt_allow_form'] = array(
      '#type'               => 'checkbox',
      '#title'                => t('Personal slcontact_xt form'),
      '#default_value'  => !empty($edit['slcontact_xt_allow_form']) ? $edit['slcontact_xt_allow_form'] : FALSE,
      '#description'      => t('Allow other users to see your profile infos via <a href="@url"> your personal contact form</a>.', array('@url' => url("user/$account->uid/slcontact_xt"))),
    );
    $form['slcontact_xt']['slcontact_xt_allow_messages'] = array(
      '#type'               => 'checkbox',
      '#title'                => t('Allow sending messages'),
      '#default_value'  => !empty($edit['slcontact_xt_allow_messages']) ? $edit['slcontact_xt_allow_messages'] : FALSE,
      '#description'      => t('Allow other users to send you ims via <a href="@url"> your personal contact form</a>.', array('@url' => url("user/$account->uid/slcontact_xt"))),
    );
    $form['slcontact_xt']['slcontact_xt_show_home'] = array(
      '#type'               => 'checkbox',
      '#title'                => t('Show Home'),
      '#default_value'  => !empty($edit['slcontact_xt_show_home']) ? $edit['slcontact_xt_show_home'] : FALSE,
      '#description'      => t('Allow other users to see your home position'),
    );
    $form['slcontact_xt']['slcontact_xt_show_position'] = array(
      '#type'               => 'checkbox',
      '#title'                => t('Show Position'),
      '#default_value'  => !empty($edit['slcontact_xt_show_position']) ? $edit['slcontact_xt_show_position'] : FALSE,
      '#description'      => t('Allow the users to see your actual position.'),
    );
    $global_maxlength = variable_get('slcontact_xt_global_msg_maxlength', 0);
    $form['slcontact_xt']['slcontact_xt_user_msg_maxlength'] = array(
      '#type'                => 'textfield',
      '#title'                 => t('Message max length'),
      '#size'                 =>3,
      '#default_value'   => !empty($edit['slcontact_xt_user_msg_maxlength']) ? $edit['slcontact_xt_user_msg_maxlength'] : $global_maxlength,
      '#description'       => t('Define the max length used for the im messages. Maximum value defined by the admins is !global_maxlength. Unlimited = 0.', array('!global_maxlength' =>$global_maxlength)),
    );
    $form['slcontact_xt']['slcontact_xt_friendslist_size'] = array(
    '#type'               => 'select',
    '#title'                => t('Friendslist size'),
    '#options'           => drupal_map_assoc(array(1, 5, 10, 20, 30, 40, 50, 100)),
    '#default_value'  => !empty($edit['slcontact_xt_friendslist_size']) ? $edit['slcontact_xt_friendslist_size'] : 5,
    '#description'      => t('The number of friends to display in the friends list.'),
  );
    return $form;
  }
  elseif ($op == 'validate') {
    return array(
      'slcontact_xt_expiration_date'          => isset($edit['slcontact_xt_expiration_date']) ? $edit['slcontact_xt_expiration_date'] : slcontact_xt_date2array(),
      'slcontact_xt_allow_form'                 => isset($edit['slcontact_xt_allow_form']) ? $edit['slcontact_xt_allow_form'] : FALSE,
      'slcontact_xt_allow_messages'         => isset($edit['slcontact_xt_allow_messages']) ? $edit['slcontact_xt_allow_messages'] : FALSE,
      'slcontact_xt_show_home'                => isset($edit['slcontact_xt_show_home']) ? $edit['slcontact_xt_show_home'] : FALSE,
      'slcontact_xt_show_position'             => isset($edit['slcontact_xt_show_position']) ? $edit['slcontact_xt_show_position'] : FALSE,
      'slcontact_xt_user_msg_maxlength'  => isset($edit['slcontact_xt_user_msg_maxlength']) ? $edit['slcontact_xt_user_msg_maxlength'] : 50,
      'slcontact_xt_friendslist_size'            => isset($edit['slcontact_xt_friendslist_size']) ? $edit['slcontact_xt_friendslist_size'] : 5
    );
  }
  elseif ($op == 'insert') {
    $edit['slcontact_xt']                                    = variable_get('slcontact_xt_default_status', 1);
    $edit['slcontact_xt_expiration_date']           = slcontact_xt_date2array();
    $edit['slcontact_xt_allow_form']                  = FALSE;
    $edit['slcontact_xt_allow_messages']          = FALSE;
    $edit['slcontact_xt_show_home']                 = FALSE;
    $edit['slcontact_xt_show_position']              = FALSE;
    $edit['slcontact_xt_user_msg_maxlength']    = 50;
    $edit['slcontact_xt_friendslist_size']              = 5;
  }
  elseif ($op == 'delete') {
    // load the slcontact_xt
    $slcontact_xt = slcontact_xt_load_slcontact_xt(array('uid' =>$account->uid));

    // delete the slcontact_xt
    slcontact_xt_delete_slcontact_xt($slcontact_xt ->id);
    return;
  }
  else if ($op == 'login') {
    // load the slcontact_xt
    $slcontact_xt = slcontact_xt_load_slcontact_xt(array('uid' =>$account->uid));

    // dont block people without slcontact_xt profile and admin
    if (!$slcontact_xt->id || $uid == 1) {
      return;
    }

    // dont block people if site access is free
    if (variable_get('slcontact_xt_pay2access_amount', 0) == 0) {
      return;
    }

    // get the actual time stamp
    $now  = strtotime("now");

    // check the expiration date
    $expiration_date = strtotime(slcontact_xt_array2date($account->slcontact_xt_expiration_date));
    if (($expiration_date - $now) > 0) {
      return;
    }

    // block the user
    db_query("UPDATE {users} SET status = 0 WHERE uid = %d", $account->uid);
    watchdog('user', 'SlContact_xt : Blocked user %name because account has expired.', array('%name' => check_plain($account->name)));

    // send a notification email
    drupal_mail('slcontact_xt', 'registration expired', $account->mail, user_preferred_language($user));

    // set messages
    watchdog('user', 'Session closed for %name.', array('%name' => $account->name));
    drupal_set_message(t('The username %name has been blocked. Your account has expired. We sent you an email containg more infos.', array('%name' => $account->name)), 'error');

    // logout the user
    session_destroy();
    module_invoke_all('user', 'logout', NULL, $account);

    // Load the anonymous user
    $account = drupal_anonymous_user();

    //drupal_goto();
  }
  
}
/**
* Determine if a user can access to the contact tab.
*/
function _slcontact_xt_user_tab_access($account) {
  if (!isset($account->slcontact_xt_allow_form)) {
    $account->slcontact_xt_allow_form = FALSE;
  }
  return user_edit_access($account) || $account->slcontact_xt_allow_form;
}
/**
* Personal contact page.
*/
function slcontact_xt_user_page($account) {

  // check if the user has a slcontact_xt
  $slcontact_xt = slcontact_xt_load_slcontact_xt(array('uid' =>$account->uid));
  if (empty($slcontact_xt->id)) {
    drupal_set_message(t('User not registered.'));
    return '';
  }

  // get the profile image
  // TODO : uncomment this when the new framework will be validated
  //$profile_image = secondlife_extract_picture_key(secondlife_get_linden_user_profile($slcontact_xt->user_key));

  // slcontact_xt base values
  //$output .= '<div><img src="http://secondlife.com/app/image/'. $profile_image. '/1" alt="profile image"/></div>';
  $output .= '<div><b><u>'. t('User Name'). ' :</u></b> '. $slcontact_xt->user_name. '</div>';
  $output .= '<div><b><u>'. t('User Key'). ' :</u></b> '. $slcontact_xt->user_key. '</div>';

  // show expiration date
  if (variable_get('slcontact_xt_pay2access_amount', 0) > 0 && $account->uid == $slcontact_xt->uid) {
    $output .= '<div><b><u>'. t('Your account is available until'). ' :</u></b> '. slcontact_xt_array2date($account->slcontact_xt_expiration_date). '</div>';
  }

  // home
  if ($slcontact_xt->account->slcontact_xt_show_home || user_access('administer users')) {
    $output .= '<div><b><u>'. t('Home'). ' :</u></b></div>';
    $output .= '<ul>';
    $output .= '<li>'. t('Home name'). ' : '. $slcontact_xt->home_name. '</li>';
    $output .= '<li>'. t('Home region'). ' : '. $slcontact_xt->home_region. '</li>';
    $output .= '<li>'. t('Home position'). ' : '. $slcontact_xt->home_position. '</li>';
    $output .= '</ul>';
  }

  // position
  if ($slcontact_xt->account->slcontact_xt_show_position || user_access('administer users')) {
    $output .= '<div><b><u>'. t('Position'). ' :</u></b></div>';
    $output .= '<ul>';
    $output .= '<li>'. t('Position date'). ' : '. $slcontact_xt->online_date. '</li>';
    $output .= '<li>'. t('User region'). ' : '. $slcontact_xt->user_region. '</li>';
    $output .= '<li>'. t('User position'). ' : '. $slcontact_xt->user_position. '</li>';
    $output .= '</ul>';
  }

  // form
  $output .= drupal_get_form('slcontact_xt_user_page_form', $account->uid);

  return $output;
}
/**
* User page slcontact_xt form
*/
function slcontact_xt_user_page_form(&$form_state, $account_uid = null) {
  global $user;
  $form = array();

  // init some values
  if (!empty($account_uid)) {
    $form_state['post']['account_uid'] = $account_uid;
  }
  if (!isset($form_state['post']['page'])) {
    $form_state['post']['page'] = 0;
  }

  // get the slcontact_xt
  $slcontact_xt = slcontact_xt_load_slcontact_xt(array('uid' =>$form_state['post']['account_uid']));

  // add some usefull elements
  $form['account_uid'] = array(
    '#type'                => 'hidden',
    '#value'              => $slcontact_xt->uid,
  );
  $form['slcontact_xt_id'] = array(
    '#type'                => 'hidden',
    '#value'              => $slcontact_xt->id,
  );

  // add the friendslist
  if ($user->uid == $slcontact_xt->uid || user_access('administer users')) {
    // get the friendslist
    $friendslist = slcontact_xt_get_friendslist($slcontact_xt, $form_state['post']['page']);
    if (count($friendslist)>0) {
      $form += slcontact_xt_friendslist($account, $slcontact_xt, $friendslist, $form_state);
    }
  }

  // check the message flood
  if (!flood_is_allowed('slcontact_xt', variable_get('slcontact_xt_hourly_threshold', 3)) && !user_access('administer users')) {
    $form['message'] = array(
      '#type'                => 'item',
      '#value'              => '<div class="messages status">'. t("You cannot send more than %number messages per hour. Please try again later.", array('%number' => variable_get('slcontact_xt_hourly_threshold', 3))). '</div>',
    );
  }
  else {
    // im form
    if ($slcontact_xt->account->slcontact_xt_allow_messages  || user_access('administer users') || ($user->uid == $slcontact_xt->uid && count($friendslist)>0)) {
      $form += slcontact_xt_im_message($slcontact_xt);
    }
  }

  return $form;
}