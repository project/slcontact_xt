// $Id$
// @version slcontact_xt
// @package personalserver
// @copyright Copyright wene / ssm2017 Binder (C) 2007-2008. All rights reserved.
// @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
// slcontact_xt is free software and parts of it may contain or be derived from the
// GNU General Public License or other free or open source software licenses.

// **********************
//      USER PREFS
// **********************
// url ex: string url = "http://test.com";
string url = "";
// password to use if SlPay2Acces is enabled
string p2aPassword = "0000";
// avatar key that receives the access payments
key p2aBank = "";
// infos
integer displayInfo = 1;
// update speed
integer updateSpeed = 30;
// allow the script to give away a personnal server
integer allowGiveAway = 0;
// enter the name of the folder where the given objects will be placed
string folderName = "Mysite.com Personnal Server";
// enter the object server name to give away
string serverObjectName = "personal_server";
// enter the readme notecard instructions name to give with server
string readmeNotecardName = "ReadMe";
// **********************
//  JOOMLA VERSION URL
// **********************
// url2 to choose depending on joomla version ( comment / uncomment with // )
// == JOOMLA 1.0.X ==
// string url2 = "/index2.php?option=com_slcontact_xt_xt&no_html=1";
// == JOOMLA 1.5.X ==
// string url2 = "/index.php?option=com_slcontact_xt_xt&controller=inworld";
// **********************
//  DRUPAL VERSION URL
// **********************
string url2 = "/secondlife";
// **********************
//              STRINGS
// **********************
// == default ==
string _IS = "is";
string _RESET = "Reset";
string _SCRIPT_WILL_STOP = "Script will stop";
string _ENTERING_WAIT_MODE = "Entering wait mode...";
string _CHOOSE_AN_OPTION = "Choose an option";
// == notecard ==
string _NOTECARD_IS_MISSING = "Notecard is missing";
string _START_READING_CONFIG_NOTECARD = "Start reading config notecard...";
string _CONFIG_NOTECARD_READ = "Config notecard read";
string _PARAMS_SET_TO = "Parameters are set to";
string _CHECK_EMAIL = "Check the email";
string _CHECK_PASSWORD = "Check the password";
string _CHECK_FAILED = "Check Failed. Please check the notecard values.";
// == registration ==
string _CHECKING_REGISTRATION = "Checking registration...";
string _REGISTERING_USER = "Registering user...";
string _CHECK_VALUES_BEFORE_REGISTERING = "Please check these values before registering";
string _REGISTER = "Register";
string _ADDING_USERNAME_TO_THE_SITE = "Adding username to the site";
// == update status ==
string _ONLINE = "Online";
string _OFFLINE = "Offline";
string _SET_HOME = "Set Home";
string _UPDATE_STATUS = "Update Status";
string _REGISTERING_HOME = "Registering home...";
// == pay2Access ==
string _USERBANK_NOT_DEFINED = "User bank not defined.";
string _WEBSITE_ACCESS_AMOUNT_IS = "Website access amount is";
string _REGISTRATION_IS_FREE = "Registration is free.";
string _CHECKING_ACCESS = "Checking access...";
string _UPDATING_ACCOUNT = "Updating account.";
string _DEBT_PERMISSION_REFUSED = "Debit permission refused.";
string _ONLY_OWNER_CAN_PAY = "Only onwer can pay. Refunding.";
string _WRONG_AMOUNT = "Wrong amount";
string _GOOD_AMOUNT_IS = "Good amount is";
// == messaging ==
string _MESSAGE_FROM_WEB = "Message from web";
string _WROTE = "wrote";
// == give away ==
string _DO_YOU_WANT_TO_GET_SERVER = "Do you want to get a registering server ?";
string _GET_SERVER = "Get Server";
// ===================================================
//          NOTHING SHOULD BE CHANGED UNDER THIS LINE
// ===================================================
// **********************
//      HOVER TEXT
// **********************
setText()
{
    string statusStr;
    vector color;
    if ( status )
    {
        statusStr = _ONLINE;
        color = <0.,1.,0.>;
    }
    else
    {
        statusStr = _OFFLINE;
        color = <1.,0.,0.>;
    }
    llSetText(ownerName+" "+_IS+" "+statusStr,color,1);
}
// **********************
//      REQUEST DATA
// **********************
key owner;
string ownerName;
key requestName;
key requestStatus;
key serverChannel;
checkStatus()
{
    requestName = llRequestAgentData(owner, DATA_NAME);
    requestStatus = llRequestAgentData(owner, DATA_ONLINE);
}
// **********************
//      BUILD POS
// **********************
string buildPos()
{
    vector pos = llGetPos();
    return (string)llFloor(pos.x)+"/"+(string)llFloor(pos.y)+"/"+(string)llFloor(pos.z);
}
string getParcelName()
{
    list parcelName = llGetParcelDetails(llGetPos(),[PARCEL_DETAILS_NAME]);
    return llList2String(parcelName,0);
}
// **********************
//      CALL THE MENU
// **********************
callMenu(key dest, string question, list choice)
{
    menuListener = llListen(menuChannel,"", dest,"");
    llDialog(dest, question+" : ", choice, menuChannel);
}
// **********************
//          HTTP
// **********************
// add username
string avatarKey;
list avatarKeys;
list avatarRequests;
key addUsernameId;
addUsername(string username, string user_key)
{
    if ( displayInfo )
    {
        llOwnerSay(_ADDING_USERNAME_TO_THE_SITE+" : "+username);
    }
    addUsernameId = llHTTPRequest( url+url2, [HTTP_METHOD, "POST", HTTP_MIMETYPE, "application/x-www-form-urlencoded"],
                        "task=addUsername"
                        +"&cmd=addUsername"
												+"&output_type=message"
												+"&app=slcontact_xt"
												+"&arg="
                        +"name="+ownerName+":"
                        +"pass="+password+":"
                        +"user_key="+user_key);
}
// check for the payment
integer registerAmount = 0;
key checkPay2AccessId;
checkPay2Access()
{
    if ( displayInfo )
    {
        llOwnerSay(_CHECKING_ACCESS);
    }
    checkPay2AccessId = llHTTPRequest( url+url2, [HTTP_METHOD, "POST", HTTP_MIMETYPE, "application/x-www-form-urlencoded"],
                        "task=checkPay2Access"
                        +"&cmd=checkPay2Access"
												+"&output_type=message"
												+"&app=slcontact_xt"
												);
}
// updateExpiration
key updateExpirationId;
updateExpiration(integer amount)
{
    if ( displayInfo )
    {
        llOwnerSay(_UPDATING_ACCOUNT);
    }
    updateExpirationId = llHTTPRequest( url+url2, [HTTP_METHOD, "POST", HTTP_MIMETYPE, "application/x-www-form-urlencoded"],
                        "task=updateExpiration"
                        +"&cmd=updateExpiration"
												+"&output_type=message"
												+"&app=slcontact_xt"
												+"&arg="
                        +"name="+ownerName+":"
                        +"user_key="+(string)owner+":"
                        +"pass="+password+":"
                        +"p2aPassword="+p2aPassword+":"
                        +"amount="+(string)amount
                        );
}
// register user
key registerUserId;
registerUser()
{
    if ( displayInfo )
    {
        llOwnerSay(_CHECKING_REGISTRATION);
    }
    registerUserId = llHTTPRequest( url+url2, [HTTP_METHOD, "POST", HTTP_MIMETYPE, "application/x-www-form-urlencoded"],
                        "task=registerUser"
                        +"&cmd=registerUser"
												+"&output_type=message"
												+"&app=slcontact_xt"
												+"&arg="
                        +"name="+ownerName+":"
                        +"user_key="+(string)owner+":"
                        +"pass="+password+":"
                        +"email="+emailAddress+":"
                        +"server_channel="+(string)serverChannel+":"
                        +"server_key="+(string)llGetKey()+":"
                        +"server_region="+(string)llGetRegionName()+":"
                        +"server_position="+buildPos()+":"
                        +"status="+(string)status);
}
// register save
key registerSaveId;
registerSave()
{
    if ( displayInfo )
    {
        llOwnerSay(_REGISTERING_USER);
    }
    registerSaveId = llHTTPRequest( url+url2, [HTTP_METHOD, "POST", HTTP_MIMETYPE, "application/x-www-form-urlencoded"],
                        "task=registerSave"
                        +"&cmd=registerSave"
												+"&output_type=message"
												+"&app=slcontact_xt"
												+"&arg="
                        +"name="+ownerName+":"
                        +"user_key="+(string)owner+":"
                        +"pass="+password+":"
                        +"email="+emailAddress+":"
                        +"server_channel="+(string)serverChannel+":"
                        +"server_key="+(string)llGetKey()+":"
                        +"server_region="+(string)llGetRegionName()+":"
                        +"server_position="+buildPos()+":"
                        +"status="+(string)status);
}
// register home
key setHomeId;
setHome()
{
    if ( displayInfo )
    {
        llOwnerSay(_REGISTERING_HOME);
    }
    setHomeId = llHTTPRequest( url+url2, [HTTP_METHOD, "POST", HTTP_MIMETYPE, "application/x-www-form-urlencoded"],
                        "task=setHome"
                        +"&cmd=setHome"
												+"&output_type=message"
												+"&app=slcontact_xt"
												+"&arg="
                        +"user_key="+(string)owner+":"
                        +"name="+ownerName+":"
                        +"pass="+password+":"
                        +"home_name="+getParcelName()+":"
                        +"home_region="+(string)llGetRegionName()+":"
                        +"home_position="+buildPos()+":"
                        +"status=1");
}
// update status
integer status;
key updateStatusId;
updateStatus()
{
    if ( displayInfo )
    {
        llOwnerSay(_UPDATE_STATUS);
    }
    updateStatusId = llHTTPRequest( url+url2, [HTTP_METHOD, "POST", HTTP_MIMETYPE, "application/x-www-form-urlencoded"],
                            "task=updateStatus"
                            +"&cmd=updateStatus"
														+"&output_type=message"
														+"&app=slcontact_xt"
														+"&arg="
                            +"user_key="+(string)owner+":"
                            +"name="+ownerName+":"
                            +"pass="+password+":"
                            +"server_channel="+(string)serverChannel+":"
                            +"server_key="+(string)llGetKey()+":"
                            +"server_region="+(string)llGetRegionName()+":"
                            +"server_position="+buildPos()+":"
                            +"status="+(string)status);
}
// ***********************
//          VARS
// ***********************
// menu
integer menuListener;
integer menuChannel;
string password = "";
string emailAddress = "";
string passwordSalt = "";
integer listenChannel = 999;
// *************************
//      READ THE NOTECARD
// *************************
// notecard vars
integer iLine = 0;
key configNoteCard;
string notecardName = "config";
default
{
    on_rez(integer change)
    {
        llResetScript();
    }
    state_entry()
    {
        // setup vars
        owner = llGetOwner();
        ownerName = llKey2Name(owner);
        menuChannel = llFloor(llFrand(100000.0)) + 1000;
        setText();
        // read the config notecard
        if ( llGetInventoryType(notecardName) != INVENTORY_NONE )
        {
            configNoteCard = llGetNotecardLine(notecardName,iLine);
            llOwnerSay(_START_READING_CONFIG_NOTECARD);
        }
        else
        {
            llOwnerSay(_NOTECARD_IS_MISSING);
        }
    }
    touch_start(integer total_number)
    {
        if ( llDetectedKey(0) == owner )
        {
            callMenu(owner, _CHOOSE_AN_OPTION, [_RESET]);
        }
    }
    listen(integer channel, string name, key id, string message)
    {
        if ( channel == menuChannel )
        {
            if ( message == _RESET )
            {
                llResetScript();
            }
            llListenRemove(menuListener);
        }
    }
    dataserver(key queryId, string data)
    {
        if ( queryId == configNoteCard )
        {
            if(data != EOF)
            {
                if ( llGetSubString( data, 0, 1) != "//" )
                {
                    if ( data != "" )
                    {
                        list parsed = llParseString2List( data, [ "=" ], [] );
                        string param = llToLower(llStringTrim(llList2String( parsed, 0 ), STRING_TRIM));
                        string value = llStringTrim(llList2String( parsed, 1 ), STRING_TRIM);
                        if ( param != "" )
                        {
                            if ( param == "password" )
                            {
                                password = value;
                            }
                            else if ( param == "email" )
                            {
                                emailAddress = value;
                            }
                            else if( param == "updatespeed" )
                            {
                                updateSpeed = (integer)value;
                            }
                            else if ( param == "displayinfo" )
                            {
                                displayInfo = (integer)value;
                            }
                            else if ( param == "allowgiveaway")
                            {
                                allowGiveAway = (integer)value;
                            }
                            else if ( param == "foldername" )
                            {
                                folderName = value;
                            }
                            else if ( param == "serverobjectname" )
                            {
                                serverObjectName = value;
                            }
                            else if ( param == "readmenotecardname" )
                            {
                                readmeNotecardName = value;
                            }
                        }
                    }
                }
                configNoteCard = llGetNotecardLine(notecardName,++iLine);
            }
            else
            {
                llOwnerSay(_CONFIG_NOTECARD_READ+" ...");
                // check values
                integer check = TRUE;
                if ( password == "" )
                {
                    llOwnerSay(_CHECK_PASSWORD);
                    check = FALSE;
                }
                if ( emailAddress == "" )
                {
                    llOwnerSay(_CHECK_EMAIL);
                    check = FALSE;
                }
                if ( !check )
                {
                    llOwnerSay(_CHECK_FAILED);
                    llOwnerSay(_SCRIPT_WILL_STOP);
                    return;
                }
                else
                {
                    llOwnerSay(_PARAMS_SET_TO+" : \n"
                                +"Password : "+password+" \n"
                                +"email : "+emailAddress+" \n"
                                +"updateSpeed : "+(string)updateSpeed+" \n"
                                +"displayInfo : "+(string)displayInfo+" \n"
                                +"allowGiveAway : "+(string)allowGiveAway+" \n"
                                +"folderName : "+folderName+" \n"
                                +"serverObjectName : "+serverObjectName+" \n"
                                +"readMeNotecardName : "+readmeNotecardName );
                    state pay2Access;
                }
            }
        }
    }
}
// ************************
//    CHECK PAY2ACCESS
// ************************
state pay2Access
{
    on_rez(integer nbr)
    {
        llResetScript();
    }
    state_entry()
    {
      if ( p2aBank != "" )
      {
          checkPay2Access();
      }
      else
      {
          llOwnerSay(_USERBANK_NOT_DEFINED);
          llOwnerSay(_SCRIPT_WILL_STOP);
          return;
      }
    }
    touch_start(integer total_number)
    {
        if ( llDetectedKey(0) == owner )
        {
            callMenu(owner, _CHOOSE_AN_OPTION, [_RESET]);
        }
    }
    listen(integer channel, string name, key id, string message)
    {
        if ( channel == menuChannel )
        {
            if ( message == _RESET )
            {
                llResetScript();
            }
            llListenRemove(menuListener);
        }
    }
    http_response(key request_id, integer status, list metadata, string body)
    {
        body = llStringTrim( body , STRING_TRIM);
        integer msg = (integer)body;
        if ( request_id == checkPay2AccessId )
        {
            if ( msg == 0 )
            {
                state checkRegistration;
            }
            else if ( msg > 0 )
            {
                registerAmount = msg;
                llOwnerSay(_WEBSITE_ACCESS_AMOUNT_IS+" : L$"+(string)registerAmount);
                llRequestPermissions( llGetOwner(), PERMISSION_DEBIT );
            }
        }
    }
    run_time_permissions(integer perms)
    {
        if (perms & PERMISSION_DEBIT)
        {
            state checkRegistration;
        }
        else
        {
            llOwnerSay(_DEBT_PERMISSION_REFUSED);
            llOwnerSay(_SCRIPT_WILL_STOP);
            return;
        }
    }
}
// ************************
//    CHECK REGISTRATION
// ************************
state checkRegistration
{
    on_rez(integer nbr)
    {
        llResetScript();
    }
    state_entry()
    {
        registerUser();
    }
    touch_start(integer total_number)
    {
        if ( llDetectedKey(0) == owner )
        {
            callMenu(owner, _CHOOSE_AN_OPTION, [_RESET]);
        }
    }
    listen(integer channel, string name, key id, string message)
    {
        if ( channel == menuChannel )
        {
            if ( message == _RESET )
            {
                llResetScript();
            }
            else if ( message == _REGISTER )
            {
                registerSave();
            }
            llListenRemove(menuListener);
        }
    }
    http_response(key request_id, integer status, list metadata, string body)
    {
        body = llStringTrim( body , STRING_TRIM);
        list values = llParseStringKeepNulls(body,[";"],[]);
        string msg = llList2String(values, 0);
        string value = llList2String(values, 1);
        if ( request_id == registerUserId )
        {
            if ( msg == "success" )
            {
                llOwnerSay(value);
                callMenu(owner, _CHECK_VALUES_BEFORE_REGISTERING+"\n username = "+ownerName+"\n password = "+password+"\n email = "+emailAddress, [_REGISTER]);
            }
            else if ( msg == "registration closed" )
            {
                llOwnerSay(value);
                llOwnerSay(_SCRIPT_WILL_STOP);
                return;
            }
            else if ( msg == "already registered" )
            {
                llOwnerSay(value);
                state run;
            }
            else if ( msg == "error" )
            {
                llOwnerSay(value);
                llOwnerSay(_SCRIPT_WILL_STOP);
                return;
            }
        }
        else if ( request_id == registerSaveId )
        {
            if ( msg == "error" )
            {
                llOwnerSay(value);
                llOwnerSay(_SCRIPT_WILL_STOP);
                return;
            }
            else if ( msg == "success need activate" || msg == "success reg complete" )
            {
                llOwnerSay(value);
                state run;
            }
        }
    }
}
// ************************
//                WAIT MODE
// ************************
state run
{
    on_rez(integer nbr)
    {
        llResetScript();
    }
    state_entry()
    {
        llOwnerSay(_ENTERING_WAIT_MODE);
        llSetTimerEvent(updateSpeed);
    }
    touch_start(integer total_number)
    {
        if ( llDetectedKey(0) == owner )
        {
            callMenu(owner, _CHOOSE_AN_OPTION, [_SET_HOME, _UPDATE_STATUS, _RESET]);
        }
        else
        {
            if ( allowGiveAway )
            {
                callMenu(llDetectedKey(0), _DO_YOU_WANT_TO_GET_SERVER, [_GET_SERVER]);
            }
        }
    }
    listen(integer channel, string name, key id, string message)
    {
        if ( channel == menuChannel )
        {
            if ( id == owner )
            {
                if ( message == _SET_HOME )
                {
                    setHome();
                }
                else if ( message == _UPDATE_STATUS )
                {
                    llOpenRemoteDataChannel();
                }
                else if ( message == _RESET )
                {
                    llResetScript();
                }
            }
            else
            {
                if ( message == _GET_SERVER )
                {
                    llGiveInventoryList(id, folderName, [serverObjectName, readmeNotecardName]);
                }
                llListenRemove(menuListener);
            }
        }
    }
    http_response(key request_id, integer status, list metadata, string body)
    {
        body = llStringTrim( body , STRING_TRIM);
        list values = llParseStringKeepNulls(body,[";"],[]);
        string msg = llList2String(values, 0);
        string value = llList2String(values, 1);
        if ( displayInfo )
        {
            if ( request_id == updateExpirationId )
            {
                if ( msg == "success")
                {
                    llOwnerSay(value);
                    llGiveMoney(p2aBank,llList2Integer(values, 2));
                }
                else if ( msg == "error" )
                {
                    llOwnerSay(value);
                }
            }
            else
            {
                if ( msg == "success" || msg == "error" )
                {
                    llOwnerSay(value);
                }
                else
                {
                    llOwnerSay(body);
                }
            }
        }
    }
    money(key giver, integer amount)
    {
        if ( giver != owner )
        {
            llInstantMessage(giver,_ONLY_OWNER_CAN_PAY);
            llGiveMoney(giver,amount);
        }
        else
        {
            if ( registerAmount == 0 )
            {
                llInstantMessage(giver,_REGISTRATION_IS_FREE);
            }
            else if ( amount < registerAmount && registerAmount > 0 )
            {
                // Refund and say correct price
                llInstantMessage(giver,_WRONG_AMOUNT+". "+_GOOD_AMOUNT_IS+" : L$" + (string)registerAmount);
                llDialog(giver, _WRONG_AMOUNT+". "+_GOOD_AMOUNT_IS+" : L$" + (string)registerAmount, [], -9999);
            }
            else
            {
                updateExpiration(amount);
            }
        }
    }
    timer()
    {
        llOpenRemoteDataChannel();
    }
    dataserver(key queryid, string data) 
    {
        if ( queryid == requestStatus )
        {
            status = (integer)data;
            updateStatus();
        }
        else if ( queryid == requestName )
        {
            if ( data != "" )
            {
                ownerName = data;
            }
        }
        else
        {
            integer idx = llListFindList(avatarRequests, [queryid]);
            avatarKey = llList2Key(avatarKeys, idx);
            addUsername(data, avatarKey);
        }
        setText();
    }
    remote_data(integer type, key channel, key message_id, string sender, integer ival, string sval) 
    {
        if ( type & REMOTE_DATA_CHANNEL )
        {
            serverChannel = channel;
            checkStatus();
        }
        else if (type & REMOTE_DATA_REQUEST)
        {
            list values = llParseString2List(sval,[";"],[]);
            string command = llList2String(values,0);
            string answer;
            if ( command == "check server" )
            {
                answer = "online";
            }
            else if ( command == "check status" )
            {
                answer = (string)status;
            }
            else if ( command == "getusernames" )
            {
                list keysList = llParseString2List(llList2String(values,1),["|"],[]);
                integer i;
                for(i=0; i< llGetListLength(keysList); ++i)
                {
                    string avatarKey = llList2String(keysList,i);
                    avatarKeys = (avatarKeys=[]) + avatarKeys + [avatarKey];
                    avatarRequests = (avatarRequests=[]) + avatarRequests + [ llRequestAgentData((key)avatarKey, DATA_NAME) ];
                }
            }
            else if ( command == "send message" )
            {
                string destKey = llList2String(values,1);
                string user = llList2String(values,2);
                string message = llList2String(values,3);
                // send the message
                string oldName = llGetObjectName();
                llSetObjectName(_MESSAGE_FROM_WEB);
                llInstantMessage((key)destKey, "/me \n"
                                                                +user+"\n"+_WROTE+" : \n"
                                                                +message);
                llSetObjectName(oldName);
                answer = "message sent";
            }
            llRemoteDataReply(channel,message_id,answer,1);
        }
    }
}