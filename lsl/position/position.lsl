// $Id$
// @version slcontact_xt
// @package position
// @copyright Copyright wene / ssm2017 Binder (C) 2007-2008. All rights reserved.
// @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
// slcontact_xt is free software and parts of it may contain or be derived from the
// GNU General Public License or other free or open source software licenses.

// **********************
//      USER PREFS
// **********************
// url ex: string url = "http://test.com";
string url = "";
// infos
integer displayInfo = 1;
// time between tests
float updateSpeed = 20;
// show position to the website
integer showPosition = 1;
// **********************
//  JOOMLA VERSION URL
// **********************
// url2 to choose depending on joomla version ( comment / uncomment with // )
// == JOOMLA 1.0.X ==
// string url2 = "/index2.php?option=com_slcontact_xt_xt&no_html=1";
// == JOOMLA 1.5.X ==
// string url2 = "/index.php?option=com_slcontact_xt_xt&controller=inworld";
// **********************
//  DRUPAL VERSION URL
// **********************
string url2 = "/secondlife";
// **********************
//              STRINGS
// **********************
string _RESET = "Reset";
string _UPDATE_POSITION = "Update Position";
string _NOTECARD_IS_MISSING = "Notecard is missing";
string _START_READING_CONFIG_NOTECARD = "Start reading config notecard...";
string _CONFIG_NOTECARD_READ = "Config notecard read";
string _CHECK_PASSWORD = "Check the password";
string _CHECK_FAILED = "Check Failed. Please check the notecard values.";
string _SCRIPT_WILL_STOP = "Script will stop";
string _ENTERING_WAIT_MODE = "Entering wait mode...";
string _ADD_FRIEND = "Add friend";
string _CHOOSE_AN_OPTION = "Choose an option";
string _DO_YOU_WANT_TO_ADD = "Do you want to add";
string _AS_FRIEND = "as friend";
string _ADDING_FRIEND = "Adding friend";
// ==============================================
//      NOTHING SHOULD BE CHANGED UNDER THIS LINE
// ==============================================
// vars
string password = "";
key owner;
string ownerName;
integer menuChannel;
integer menuListener;
// **********************
//      BUILD POS
// **********************
string buildPos()
{
    vector pos = llGetPos();
    return (string)llFloor(pos.x)+"/"+(string)llFloor(pos.y)+"/"+(string)llFloor(pos.z);
}
// **********************
//          HTTP
// **********************
// Update Position
key updatePositionId;
updatePosition()
{
    if ( displayInfo )
    {
        llOwnerSay(_UPDATE_POSITION);
    }
    updatePositionId = llHTTPRequest( url+url2, [HTTP_METHOD, "POST", HTTP_MIMETYPE, "application/x-www-form-urlencoded"],
                "task=updatePosition"
                +"&cmd=updatePosition"
                +"&output_type=message"
                +"&app=slcontact_xt"
                +"&arg="
                +"user_key="+(string)llGetOwner()+":"
                +"name="+llKey2Name(llGetOwner())+":"
                +"pass="+password+":"
                +"user_region="+(string)llGetRegionName()+":"
                +"user_position="+buildPos()+":"
                +"status=1");
}
// add friend
string friendName;
string friendKey;
key addFriendId;
addFriend()
{
    if ( displayInfo )
    {
        llOwnerSay(_ADDING_FRIEND);
    }
    addFriendId = llHTTPRequest( url+url2, [HTTP_METHOD, "POST", HTTP_MIMETYPE, "application/x-www-form-urlencoded"],
                "task=addFriend"
                +"&cmd=addFriend"
                +"&output_type=message"
                +"&app=slcontact_xt"
                +"&arg="
                +"user_key="+(string)llGetOwner()+":"
                +"name="+llKey2Name(llGetOwner())+":"
                +"pass="+password+":"
                +"friend_name="+friendName+":"
                +"friend_key="+friendKey+":"
                +"status=1");
}
// *************************
//      READ THE NOTECARD
// *************************
// notecard vars
integer iLine = 0;
key configNoteCard;
string notecardName = "config";
default
{
    on_rez(integer change)
    {
        llResetScript();
    }
    state_entry()
    {
        // setup vars
        owner = llGetOwner();
        ownerName = llKey2Name(owner);
        menuChannel = llFloor(llFrand(100000.0)) + 1000;
        // read the config notecard
        if ( llGetInventoryType(notecardName) != INVENTORY_NONE )
        {
            configNoteCard = llGetNotecardLine(notecardName,iLine);
            llOwnerSay(_START_READING_CONFIG_NOTECARD);
        }
        else
        {
            llOwnerSay(_NOTECARD_IS_MISSING);
        }
    }
    touch_start(integer total_number)
    {
        if ( llDetectedKey(0) == owner )
        {
            menuListener = llListen(menuChannel,"", owner,"");
            llDialog(owner, _CHOOSE_AN_OPTION+" : ", [_RESET], menuChannel);
        }
    }
    listen(integer channel, string name, key id, string message)
    {
        if ( channel == menuChannel )
        {
            if ( message == _RESET )
            {
                llResetScript();
            }
            llListenRemove(menuListener);
        }
    }
    dataserver(key queryId, string data)
    {
        if ( queryId == configNoteCard )
        {
            if(data != EOF)
            {
                if ( llGetSubString( data, 0, 1) != "//" )
                {
                    if ( data != "" )
                    {
                        list parsed = llParseString2List( data, [ "=" ], [] );
                        string param = llToLower(llStringTrim(llList2String( parsed, 0 ), STRING_TRIM));
                        string value = llStringTrim(llList2String( parsed, 1 ), STRING_TRIM);
                        if ( param != "" )
                        {
                            if ( param == "password" )
                            {
                                password = value;
                            }
                            else if( param == "updatespeed" )
                            {
                                updateSpeed = (integer)value;
                            }
                            else if ( param == "displayinfo" )
                            {
                                displayInfo = (integer)value;
                            }
                            else if ( param == "showposition" )
                            {
                                showPosition = (integer)value;
                            }
                            else if ( param == "listenchannel" )
                            {
                                llMessageLinked( LINK_SET, 0, value, (key)"setListenChannel" );
                            }
                            else if ( param == "addfriendcommand" )
                            {
                                llMessageLinked( LINK_SET, 0, value, (key)"setAddfriendCommand" );
                            }
                        }
                    }
                }
                configNoteCard = llGetNotecardLine(notecardName,++iLine);
            }
            else
            {
                llOwnerSay(_CONFIG_NOTECARD_READ+" ...");
                // check values
                integer check = TRUE;
                if ( password == "" )
                {
                    llOwnerSay(_CHECK_PASSWORD);
                    check = FALSE;
                }
                if ( !check )
                {
                    llOwnerSay(_CHECK_FAILED);
                    llOwnerSay(_SCRIPT_WILL_STOP);
                    return;
                }
                else
                {
                    state wait;
                }
            }
        }
    }
}
// ************************
//          MAIN
// ************************
state wait
{
    on_rez(integer nbr)
    {
        llResetScript();
    }
    state_entry()
    {
        llOwnerSay(_ENTERING_WAIT_MODE);
        if ( showPosition )
        {
            llSetTimerEvent(updateSpeed);
        }
    }
    touch_start(integer total_number)
    {
        if ( llDetectedKey(0) == owner )
        {
            menuListener = llListen(menuChannel,"", owner,"");
            llDialog(owner, "Choose an option : ", [_RESET], menuChannel);
        }
    }
    listen(integer channel, string name, key id, string message)
    {
        if ( channel == menuChannel )
        {
            if ( message == _RESET )
            {
                llResetScript();
            }
            else if ( message == _ADD_FRIEND )
            {
                addFriend();
            }
            llListenRemove(menuListener);
        }
    }
    http_response(key request_id, integer status, list metadata, string body)
    {
        body = llStringTrim( body , STRING_TRIM);
        list values = llParseStringKeepNulls(body,[";"],[]);
        string msg = llList2String(values, 0);
        string value = llList2String(values, 1);
        if ( request_id == updatePositionId || request_id == addFriendId )
        {
            if ( msg == "success" )
            {
                if ( displayInfo )
                {
                    llOwnerSay(value);
                }
            }
            else if ( msg == "error" )
            {
                llOwnerSay(value);
            }
        }
    }
    link_message(integer sender_num, integer num, string str, key id)
    {
        string command = (string)id;
        list values = llParseStringKeepNulls(str,[";"],[]);
        if ( command == "setAvatar" )
        {
            friendName = llList2String(values,0);
            friendKey = llList2String(values,1);
            menuListener = llListen(menuChannel,"", owner,"");
            llDialog(owner, _DO_YOU_WANT_TO_ADD+" "+ friendName+" "+_AS_FRIEND+" : ", [_RESET, _ADD_FRIEND], menuChannel);
        }
    }
    timer()
    {
        updatePosition();
    }
}
