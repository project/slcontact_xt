// $Id$
// @version slcontact_xt
// @package position
// @copyright Copyright wene / ssm2017 Binder (C) 2007-2008. All rights reserved.
// @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
// slcontact_xt is free software and parts of it may contain or be derived from the
// GNU General Public License or other free or open source software licenses.

// **********************
//      USER PREFS
// **********************
integer SENSOR_DISTANCE = 30;
string CMD = "addfriend";
integer listenChannel = 1;
// **********************
//          STRINGS
// **********************
string _COULD_NOT_FIND = "Could not find";
string _REQUESTING_KEY_ON_WEB = "Requesting key on the web ...";
string _USAGE_MESSAGE = "Usage: Say '/addfriend [person's name]' or '/1addfriend [person's name]'";
string _NO_RESIDENT_NAMED = "No resident named";
// ===================================================
//          NOTHING SHOULD BE CHANGED UNDER THIS LINE
// ===================================================
// vars
string avTarget = "";
key owner;
key avKey = NULL_KEY;
// **********************
//          HTTP
// **********************
key requestid;
requestWeb()
{
    llOwnerSay( _COULD_NOT_FIND+" '" + avTarget + "'" );
    llOwnerSay(_REQUESTING_KEY_ON_WEB);llOwnerSay(avTarget);
    requestid = llHTTPRequest("http://w-hat.com/name2key?terse=1&name="+llEscapeURL(avTarget),[HTTP_METHOD,"GET"],"");
}
// ***********************
//              MAIN
// ***********************
default
{
    on_rez( integer startcode )
    {
        llResetScript();
    }
    state_entry()
    {
        owner = llGetOwner();
        llListen( listenChannel, "", owner, "" );
    }
    sensor(integer total_number)
    {
        integer i;
        for ( i = 0; i < total_number; i++ )
        {
            key detectedKey = llDetectedKey(i);
            string detectedName = llDetectedName(i);
            if ( llSubStringIndex(llToLower(detectedName), avTarget) >= 0 && detectedKey != owner )
            {
                avKey = detectedKey;
                // llOwnerSay((string)avKey);
                llMessageLinked( LINK_SET, 0, detectedName+";"+(string)avKey, (key)"setAvatar" );
                return;
            }
        }
        if ( avKey == NULL_KEY )
        {
            requestWeb();
        }
    }
    no_sensor()
    {
        requestWeb();
    }
    listen(integer channel,string name,key id,string message)
    {
        if ( llToLower(llGetSubString(message,0,8)) == CMD )
        {
            avTarget = llStringTrim(llGetSubString(message, 9, -1), STRING_TRIM);
            if ( llStringLength(avTarget) < 3 )
            {
                llOwnerSay(_USAGE_MESSAGE);
            }
            else
            {
                llSensor( "", NULL_KEY, AGENT, SENSOR_DISTANCE, PI );
            }
        }
        else
        {
            llOwnerSay(_USAGE_MESSAGE);
        }
    }
    link_message(integer sender_num, integer num, string str, key id)
    {
        string command = (string)id;
        if ( command == "setListenChannel" )
        {
            listenChannel = (integer)str;
        }
        else if ( command == "setAddfriendCommand" )
        {
            CMD = str;
        }
    }
    http_response( key request_id, integer status, list metadata, string body)
    {
        if (request_id == requestid)
        {
            if ( (key)body == NULL_KEY )
            {
                llOwnerSay(_NO_RESIDENT_NAMED +" "+ avTarget);
            }
            else
            {
                llMessageLinked( LINK_SET, 0, avTarget+";"+body, (key)"setAvatar" );
            }
        }
    }
}