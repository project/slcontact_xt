<?php
/**
 * @package	SlContact_xt
 * @copyright	Copyright (C) 2009 Wene / ssm2017 Binder (S.Massiaux). All rights reserved.
 * @license		GNU/GPL, http://www.gnu.org/licenses/gpl-2.0.html
 * SlContact_xt is free software. This version may have been modified pursuant to the GNU General Public License,
 * and as distributed it includes or is derivative of works licensed under the GNU General Public License
 * or other free or open source software licenses.
 */

/**
* Route commands
*/
function slcontact_xt_inworld_controller($cmd, &$sl, $args) {
  $response = array();
  switch ($cmd) {
    case 'registerUser':
      $response = slcontact_xt_check_registration($sl, $args);
      break;

    case 'registerSave':
      $response = slcontact_xt_register_user($sl, $args);
      break;

    case 'updateStatus':
      $response = slcontact_xt_update_status($sl, $args);
      break;

    case 'checkPay2Access':
      $response['status'] = TRUE;
      $response['message'] = variable_get('slcontact_xt_pay2access_amount', 0);
      break;

    case 'updateExpiration':
      $response = slcontact_xt_update_expiration($sl, $args);
      break;

    case 'setHome':
      $response = slcontact_xt_set_home($sl, $args);
      break;

    case 'updatePosition':
      $response = slcontact_xt_update_position($sl, $args);
      break;

    case 'addFriend':
      $response = slcontact_xt_addfriend($sl, $args);
      break;

    case 'addUsername':
      $response['message'] = 'already registered';
      $response['status'] = TRUE;
      break;

    default:
      $response['status'] = FALSE;
      $response['message'] = "Oops! Unknown command: $cmd.";
      break;
  }
  $sl->response['status'] = $response['status'];
  $sl->response['message'] = $response['message'];
}

/**
* Check user registration
*/
function slcontact_xt_check_registration(&$sl, $args) {
  $response = array();

  // check if registration is allowed
  if (variable_get('user_register', 0) == 0) {
    $response['status'] = FALSE;
    $response['message'] = 'registration closed;'. t('Registration is closed.');
    return $response;
  }

  // check if the user is registered on the site
  $account = user_load(array('name' =>$args['name']));
  if ($account->uid != 0) {
    // add or update the slcontact_xt
    $args['uid'] = $account->uid;
    $saved = slcontact_xt_store_slcontact_xt($args);
    if ($saved['status'] == FALSE) {
      $response['status'] = $saved['status'];
      $response['message'] = $saved['message'];
      return $response;
    }
    $response['status'] = FALSE;
    $response['message'] = 'already registered;'. t('User is already registered.');
    return $response;
  }

  // user is not registered so return success
  $response['status'] = TRUE;
  $response['message'] = 'success;'. t('Start register process.');
  return $response;
}

/**
* Register the user
*/
function slcontact_xt_register_user(&$sl, $args) {
  $reponse = array();

  // Verify the syntax of the given name.
  $check_username = user_validate_name($args['name']);
  if ($check_username != "") {
    $response['status'] = FALSE;
    $response['message'] = "error;". $check_username;
    return $response;
  }

  // verify the email
  $check_email = user_validate_mail($args['email']);
  if ($check_email != "") {
    $response['status'] = FALSE;
    $response['message'] = "error;". $check_email;
    return $response;
  }

  $name  = $args['name'];
  $mail    = $args['email'];
  $pass   = $args['pass'];
  $status = 1;

  // define if the user needs to validate email
  if (variable_get('user_register', 0) == 2 || variable_get('user_email_verification', TRUE)) {
    $status = 0;
  }

  // register the user
  $values = array(
    'name'  =>$name,
    'mail'    =>$mail,
    'pass'   =>$pass,
    'status' =>$status,
    'roles'   =>array_filter(variable_get('slcontact_xt_default_roles', array()))
  );
  $account = user_save('', $values);
  if (!$account) {
    $response['status'] = FALSE;
    $response['message'] = "error;". t("Error saving user account.");
    return $response;
  }

  // add or update the slcontact_xt
  $args['uid'] = $account->uid;
  $saved = slcontact_xt_store_slcontact_xt($args);
  $response['message'] = $saved['message'];
  $response['status'] = $saved['status'];
  if ($saved['status'] == FALSE) {
    return $response;
  }

  watchdog('user', 'SlContact_xt inworld : New user: %name (%email).', array('%name' => $name, '%email' => $mail), WATCHDOG_NOTICE, l(t('edit'), 'user/'. $account->uid .'/edit'));

  // Add plain text password into user account to generate mail tokens.
    $account->password = $pass;

  // user is registered so check if the user needs a confirmation email
  if (variable_get('user_email_verification', TRUE) && variable_get('user_register', 0) < 2) {
    // Create new user account, no administrator approval required but need to confirm email.
    _user_mail_notify('register_no_approval_required', $account);
    $response['status'] = TRUE;
    $response['message'] = "success need activate;". t('Your password and further instructions have been sent to your e-mail address.');
    return $response;
  }
  else if (variable_get('user_register', 0) == 2) {
    // Create new user account, administrator approval required.
    _user_mail_notify('register_pending_approval', $account);
    $response['status'] = TRUE;
    $response['message'] = "success need activate;". t('Thank you for applying for an account. Your account is currently pending approval by the site administrator.<br />In the meantime, a welcome message with further instructions has been sent to your e-mail address.');
    return $response;
  }
  else if ($status == 1) {
    // Create new user account, no administrator approval required.
    _user_mail_notify('register_no_approval_required', $account);
    $response['status'] = TRUE;
    $response['message'] = "success reg complete;". t('Your password and further instructions have been sent to your e-mail address.');
    return $response;
  }
}

/**
* Update the user status
*/
function slcontact_xt_update_status(&$sl, $args) {
  $response = array();

  // check if the user is registered on the site
  $account = user_load(array('name' =>$args['name']));
  if ($account->uid == 0) {
    $response['status'] = FALSE;
    $response['message'] = 'error;'. t('User not registered.');
    return $response;
  }

  // update the slcontact_xt
  $args['uid'] = $account->uid;
  $saved = slcontact_xt_store_slcontact_xt($args);
  $response['status'] = $saved['status'];
  $response['message'] = $saved['message'];
  return $response;
}

/**
* Update the expiration date
*/
function slcontact_xt_update_expiration(&$sl, $args) {
  $response = array();

  // check if the user is registered on the site
  $account = user_load(array('name' =>$args['name']));
  if ($account->uid == 0) {
    $response['status'] = FALSE;
    $response['message'] = 'error;'. t('User not registered.');
    return $response;
  }

  // check the user password
  if (md5($args['pass']) != $account->pass) {
    $response['status'] = FALSE;
    $response['message'] = 'error;'. t('Please verify the user password.');
    return $response;
  }

  // check the pay2access password
  if ($args['p2aPassword'] != variable_get('slcontact_xt_pay2access_password', '0000')) {
    $response['status'] = FALSE;
    $response['message'] = 'error;'. t('Please verify the p2a password.');
    return $response;
  }

  // get the pay2access amount
  $p2a_amount = variable_get('slcontact_xt_pay2access_amount', 0);

  if ($p2a_amount == 0) {
    $response['status'] = FALSE;
    $response['message'] = 'error;'. t('The site access is free of charge.');
    return $response;
  }

  // get the slcontact_xt
  $slcontact_xt = slcontact_xt_load_slcontact_xt(array('uid' =>$account->uid));
  if (!$slcontact_xt->id) {
    $args['ui'] = $account->uid;
    $saved = slcontact_xt_store_slcontact_xt($args);
    if ($saved['status'] == FALSE) {
      $response['status'] = $saved['status'];
      $response['message'] = $saved['message'];
      return $response;
    }
    $slcontact_xt = slcontact_xt_load_slcontact_xt(array('uid' =>$account->uid));
  }

  // get the expiration_date
  $expiration_date = strtotime(slcontact_xt_array2date($slcontact_xt->account->slcontact_xt_expiration_date));

  // check if expiration_date is null
  if (!$expiration_date) {
    $expiration_date = strtotime("now");
  }

  // compute the expiration date
  $week = (3600 * 24) * 7;
  $duration = $week * ($args['amount'] / $p2a_amount);
  $expiration_date = date('Y-m-d H:i:s', ($expiration_date + $duration));

  // update values
  $account = user_save($account, array('status' =>1, 'slcontact_xt_expiration_date' =>slcontact_xt_date2array($expiration_date)));
  if (!$account) {
    $response['status'] = FALSE;
    $response['message'] = "error;". t("Error saving user account.");
    return $response;
  }

  watchdog('slcontact_xt', 'Contact updated until %date ', array('%date' => $expiration_date));
  $response['status'] = TRUE;
  $response['message'] = "success;". t('Contact updated until'). " : ". $expiration_date. ";". $args['amount'];
  return $response;
}

/**
* Set home
*/
function slcontact_xt_set_home(&$sl, $args) {
  $response = array();

  // check if the user is registered on the site
  $account = user_load(array('name' =>$args['name']));
  if ($account->uid == 0) {
    $response['status'] = FALSE;
    $response['message'] = 'error;'. t('User not registered.');
    return $response;
  }

  // check the user password
  if (md5($args['pass']) != $account->pass) {
    $response['status'] = FALSE;
    $response['message'] = 'error;'. t('Please verify the user password.');
    return $response;
  }

  // get the slcontact_xt
  $slcontact_xt = slcontact_xt_load_slcontact_xt(array('uid' =>$account->uid));
  if (!$slcontact_xt->id) {
    $args['ui'] = $account->uid;
    $saved = slcontact_xt_store_slcontact_xt($args);
    if ($saved['status'] == FALSE) {
      $response['status'] = $saved['status'];
      $response['message'] = $saved['message'];
      return $response;
    }
    $slcontact_xt = slcontact_xt_load_slcontact_xt(array('uid' =>$account->uid));
  }

  // update values
  db_query("UPDATE {slcontact_xt} SET home_name = '%s', home_region = '%s', home_position = '%s' WHERE uid = %d", $args['home_name'], $args['home_region'], $args['home_position'], $slcontact_xt->uid);
  $response['status'] = TRUE;
  $response['message'] = 'success;'. t('Home registered.');
  return $response;
  
}

/**
* Update position
*/
function slcontact_xt_update_position(&$sl, $args) {
  $response = array();

  // check if the user is registered on the site
  $account = user_load(array('name' =>$args['name']));
  if ($account->uid == 0) {
    $response['status'] = FALSE;
    $response['message'] = 'error;'. t('User not registered.');
    return $response;
  }

  // check the user password
  if (md5($args['pass']) != $account->pass) {
    $response['status'] = FALSE;
    $response['message'] = 'error;'. t('Please verify the user password.');
    return $response;
  }

  // get the slcontact_xt
  $slcontact_xt = slcontact_xt_load_slcontact_xt(array('uid' =>$account->uid));
  if (!$slcontact_xt->id) {
    $args['ui'] = $account->uid;
    $saved = slcontact_xt_store_slcontact_xt($args);
    if ($saved['status'] == FALSE) {
      $response['status'] = $saved['status'];
      $response['message'] = $saved['message'];
      return $response;
    }
    $slcontact_xt = slcontact_xt_load_slcontact_xt(array('uid' =>$account->uid));
  }

  // build the date
  $date = date('Y-m-d H:i:s');

  // update values
  db_query("UPDATE {slcontact_xt} SET user_key = '%s', user_region = '%s', user_position = '%s', online_date = '%s' WHERE uid = %d", $args['user_key'], $args['user_region'], $args['user_position'], $date, $slcontact_xt->uid);
  $response['status'] = TRUE;
  $response['message'] = 'success;'. t('Position updated.');
  return $response;
}

/**
* Add friend
*/
function slcontact_xt_addfriend(&$sl, $args) {
  $response = array();

  // check if the user is registered on the site
  $account = user_load(array('name' =>$args['name']));
  if ($account->uid == 0) {
    $response['status'] = FALSE;
    $response['message'] = 'error;'. t('User not registered.');
    return $response;
  }

  // check the user password
  if (md5($args['pass']) != $account->pass) {
    $response['status'] = FALSE;
    $response['message'] = 'error;'. t('Please verify the user password.');
    return $response;
  }

  // check if the friend already exists
  $friend = slcontact_xt_load_slcontact_xt(array('user_name' =>$args['friend_name']));
  if (!$friend->id) {
    $saved_friend = slcontact_xt_store_slcontact_xt(array('name' =>$args['friend_name'], 'user_key' =>$args['friend_key']));
    if ($saved_friend['status'] == FALSE) {
      $response['status'] = $saved_friend['status'];
      $response['message'] = $saved_friend['message'];
      return $response;
    }
    $friend = slcontact_xt_load_slcontact_xt(array('user_name' =>$args['friend_name']));
  }

  // get the slcontact_xt
  $slcontact_xt = slcontact_xt_load_slcontact_xt(array('uid' =>$account->uid));
  if (!$slcontact_xt->id) {
    $args['ui'] = $account->uid;
    $saved = slcontact_xt_store_slcontact_xt($args);
    if ($saved['status'] == FALSE) {
      $response['status'] = $saved['status'];
      $response['message'] = $saved['message'];
      return $response;
    }
    $slcontact_xt = slcontact_xt_load_slcontact_xt(array('uid' =>$account->uid));
  }

  // check if friend is already in the contact list
  $slcontact_xt_friend = db_fetch_object(db_query('SELECT * FROM {slcontact_xt_friends} WHERE slcontact_xt_id = %d AND friend_id = %d', $slcontact_xt->id, $friend->id));
  if ($slcontact_xt_friend ->slcontact_xt_id != 0) {
    $response['status'] = FALSE;
    $response['message'] = 'error;'. t('Friend already added.');
    return $response;
  }

  // make the link between contact and friend
  db_query('INSERT INTO {slcontact_xt_friends} (slcontact_xt_id ,friend_id) VALUES (%d ,%d)', $slcontact_xt->id, $friend->id);
  $response['status'] = TRUE;
  $response['message'] = 'success;'. t('Friend added.');
  return $response;
}