<?php
/**
 * @package	SlContact_xt
 * @copyright	Copyright (C) 2009 Wene / ssm2017 Binder (S.Massiaux). All rights reserved.
 * @license		GNU/GPL, http://www.gnu.org/licenses/gpl-2.0.html
 * SlContact_xt is free software. This version may have been modified pursuant to the GNU General Public License,
 * and as distributed it includes or is derivative of works licensed under the GNU General Public License
 * or other free or open source software licenses.
 */

/**
* Send an im message
*/
function slcontact_xt_send_im($server_channel, $user_key, $from, $message) {
  $result = secondlife_rpc($server_channel, 0, "send message;". $user_key. ";". $from. ";". $message);
  if ($result["string"] == "message sent") {
    $msg = t('Your message has been sent');
  }
  else {
    $msg = t('Your message counldn\'t been sent');
  }
  return $msg;
}

/**
* Message form.
*/
function slcontact_xt_im_message($slcontact_xt) {
  global $user;
  $form = array();

  // message container
  $form['slcontact_xt_message'] = array(
    '#type' => 'fieldset',
    '#title' => t('Im message'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['slcontact_xt_message']['container'] = array(
    '#value' => '&nbsp;',
    '#prefix' => '<div id="slcontact_xt-send-message">',
    '#suffix' => '</div>',
  );

  // contact info
  $form['slcontact_xt_message']['container']['contact_information'] = array(
    '#value' => filter_xss_admin(variable_get('slcontact_xt_form_information', t('You can leave a message using the contact form below.')))
  );

  // get the message max length
  $maxlength = slcontact_xt_get_message_maxlength($slcontact_xt->account->slcontact_xt_user_msg_maxlength);

  // the message
  $form['slcontact_xt_message']['container']['name'] = array(
    '#type'                => 'textfield',
    '#title'                => t('Your name'),
    '#size'                => $maxlength,
    '#maxlength'      => $maxlength,
    '#default_value'  => $user->uid ? $user->name : '',
    '#required'          => FALSE,
  );
  $form['slcontact_xt_message']['container']['message'] = array(
    '#type'                => 'textfield',
    '#title'                => t('Message'),
    '#size'                => $maxlength,
    '#maxlength'      => $maxlength,
    '#default_value'  => t('Hello from web'),
    '#required'         => FALSE,
  );

  // submit button
  $form['slcontact_xt_message']['container']['slcontact_xt_message_submit_button'] = array(
    '#type' => 'submit',
    '#value' => t('Send Message'),
    '#weight' => 1,
    '#ahah' => array(
      'path' => 'slcontact_xt_send_message/js',
      'wrapper' => 'slcontact_xt-send-message',
      'progress' => array('type' => 'throbber', 'message' => t('Please wait...')),
      'method' => 'replace',
      'effect' => 'fade',
    ),
  );

  return $form;
}

/**
* AHAH callback to send message
*/
function slcontact_xt_send_message_js() {

  // drupal's alchemy :)
  $form_state = array('storage' => NULL, 'submitted' => FALSE);
  $form_build_id = $_POST['form_build_id'];
  $form = form_get_cache($form_build_id, $form_state);
  $args = $form['#parameters'];
  $form_id = array_shift($args);
  $form_state['post'] = $form['#post'] = $_POST;
  $form['#programmed'] = $form['#redirect'] = FALSE;

  // security check
  if (drupal_validate_utf8($form_state['post']['name']) && drupal_validate_utf8($form_state['post']['message'])) {

    // check if friends checked
    $friends = $form_state['post']['friends'];
    if (count($friends) == 0) {
      $friends[] = $form_state['post']['slcontact_xt_id'];
    }

    // get the sender slcontact_xt
    $slcontact_xt = slcontact_xt_load_slcontact_xt(array('id' =>$form_state['post']['slcontact_xt_id']));

    // get the message max length
    $maxlength = slcontact_xt_get_message_maxlength($slcontact_xt->account->slcontact_xt_user_msg_maxlength);
    if ($maxlength > 0) {
      $form_state['post']['message'] = substr($form_state['post']['message'], 0, $maxlength);
    }

    // get the friends values
    $friendslist = db_query("SELECT * FROM {slcontact_xt} WHERE id IN (". db_placeholders($friends) . ")", $friends);

    while ($friend = db_fetch_object($friendslist)) {
      if (!flood_is_allowed('slcontact_xt', variable_get('slcontact_xt_hourly_threshold', 3)) && !user_access('administer users')) {
        drupal_set_message(t("You cannot send more than %number messages per hour. Please try again later.", array('%number' => variable_get('slcontact_xt_hourly_threshold', 3))), 'error');
      }
      else {
        // register the flood
        flood_register_event('slcontact_xt');

        // send the message
        $msg = slcontact_xt_send_im($slcontact_xt->server_channel, $friend->user_key, $form_state['post']['name'], $form_state['post']['message']);
        $msg .= ' '. t('to'). ' : '. $friend->user_name;
      }
      sleep(3);
      drupal_set_message($msg);
    }
  }
  else {
    drupal_set_message(t('Message could not been sent.'), 'error');
  }
  $output = theme('status_messages');
  drupal_json(array('status' => TRUE, 'data' => $output));
}

/**
* Helper to find the message max length
*/
function slcontact_xt_get_message_maxlength($slcontact_xt_maxlength) {
  $global_maxlength = variable_get('slcontact_xt_global_msg_maxlength', 0);
  $maxlength = 0;
		if ($global_maxlength > 0) {
			$maxlength = $global_maxlength;
		}
		if ($slcontact_xt_maxlength > 0) {
			$maxlength = $slcontact_xt_maxlength;
		}
		if (($global_maxlength > 0) && ($slcontact_xt_maxlength > 0) && ($global_maxlength < $slcontact_xt_maxlength)) {
			$maxlength = $global_maxlength;
		}
		return $maxlength;
}