<?php
/**
 * @package	SlContact_xt
 * @copyright	Copyright (C) 2009 Wene / ssm2017 Binder (S.Massiaux). All rights reserved.
 * @license		GNU/GPL, http://www.gnu.org/licenses/gpl-2.0.html
 * SlContact_xt is free software. This version may have been modified pursuant to the GNU General Public License,
 * and as distributed it includes or is derivative of works licensed under the GNU General Public License
 * or other free or open source software licenses.
 */

/**
* Get a friends list
*/
function slcontact_xt_get_friendslist($slcontact_xt, $page = 0, $limit = 0) {
  // clean some values
  if ($limit == 0) {
    $limit = $slcontact_xt->account->slcontact_xt_friendslist_size;
  }

  // get the friendslist
  $friendslist = array();
  $query = "SELECT c.id,c.uid,c.user_name,u.data FROM {slcontact_xt} AS c"
             . " LEFT JOIN {users} AS u ON u.uid=c.uid"
             . " LEFT JOIN {slcontact_xt_friends} AS f ON f.slcontact_xt_id=". $slcontact_xt->id
             . " WHERE c.id=f.friend_id"
             . " ORDER BY c.user_name ASC";
  $_GET['page'] = $page;
  $query_result =  pager_query($query, $limit);
  while ($slcontact_xt_friend = db_fetch_object($query_result)) {
    $slcontact_xt_friend = drupal_unpack($slcontact_xt_friend, 'data');
    //if($slcontact_xt_friends->slcontact_xt_allow_form) {
      array_push($friendslist, $slcontact_xt_friend);
    //}
    }
  return $friendslist;
}

/**
* Friendslist form
*/
function slcontact_xt_friendslist($account, $slcontact_xt, $friendslist, $form_state) {

  $form = array();

  // friends page list
  global $pager_total, $pager_total_items, $pager_page_array;
  $options = array();
  for ($i = 0;$i<$pager_total[0];$i++) {
    array_push($options, $i);
  }

  // container
  $form['slcontact_xt_friendslist'] = array(
    '#type' => 'fieldset',
    '#title' => t('Friends list'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['slcontact_xt_friendslist']['container'] = array(
    '#value' => '&nbsp;',
    '#prefix' => '<div id="slcontact_xt-friendslist">',
    '#suffix' => '</div>',
  );

  // show the friends quantity
  if ($pager_total_items[0] > 0) {
    $form['slcontact_xt_friendslist']['container']['friendscount'] = array(
      '#type'     => 'item',
      '#value'    => ($pager_total_items[0] == 1) ? t('!qty Friend found.', array('!qty' =>$pager_total_items[0])) : t('!qty Friends found.', array('!qty' =>$pager_total_items[0])),
    );
  }

  // get the friendslist item
  slcontact_xt_friendslist_item($friendslist, $form);

  // show the page selector
  if ($pager_total_items[0] > $slcontact_xt->account->slcontact_xt_friendslist_size) {
    $form['slcontact_xt_friendslist']['page'] = array(
      '#type' => 'select',
      '#title' => t('Page'),
      '#options' => $options,
      '#description' => t('Select a friend\'s page.'),
      '#ahah' => array(
        'path' => 'slcontact_xt_get_friendslist/js',
        'wrapper' => 'slcontact_xt-friendslist',
        'method' => 'replace',
        'effect' => 'fade',
      ),
    );
  }

  // show the remove button
  if ($pager_total_items[0] > 0) {
    $form['slcontact_xt_friendslist']['container']['delete-button'] = array(
      '#type' => 'submit',
      '#value' => t('Remove friend'),
      '#weight' => 1,
      '#ahah' => array(
        'path' => 'slcontact_xt_remove_friend/js',
        'wrapper' => 'slcontact_xt-friendslist',
        'progress' => array('type' => 'throbber', 'message' => t('Please wait...')),
        'method' => 'replace',
        'effect' => 'fade',
      ),
    );
  }
// TODO : try to remove the pager when no friends ( i dont have any solution for now )
  return $form;
}

/**
* Friendslist checkbox
*/
function slcontact_xt_friendslist_item($friendslist, &$form) {
  $options = array();
  foreach ($friendslist as $friend) {
    $options[$friend->id] = $friend->user_name;
  }

  // friends checkbox list
  $form['slcontact_xt_friendslist']['container']['friends'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Friends List'),
    '#options' => $options,
    '#description' => t('Select friends to send an im message.'),
  );
}

/**
* AHAH callback to build the list
*/
function slcontact_xt_get_friendslist_js() {
  $form_state = array('storage' => NULL, 'submitted' => FALSE);
  $form_build_id = $_POST['form_build_id'];
  $form = form_get_cache($form_build_id, $form_state);
  $args = $form['#parameters'];
  $form_id = array_shift($args);
  $form_state['post'] = $form['#post'] = $_POST;
  $form['#programmed'] = $form['#redirect'] = FALSE;
  drupal_process_form($form_id, $form, $form_state);
  $form = drupal_rebuild_form($form_id, $form_state, $args, $form_build_id);
  $choice_form = $form['slcontact_xt_friendslist']['container'];
  unset($choice_form['#prefix'], $choice_form['#suffix']);
  $output = theme('status_messages') . drupal_render($choice_form);
  drupal_json(array('status' => TRUE, 'data' => $output));
}

/**
* AHAH callback to remove a friend
*/
function slcontact_xt_remove_friend_js() {
  // drupal's alchemy :)
  $form_state = array('storage' => NULL, 'submitted' => FALSE);
  $form_build_id = $_POST['form_build_id'];
  $form = form_get_cache($form_build_id, $form_state);
  $args = $form['#parameters'];
  $form_id = array_shift($args);
  $form_state['post'] = $form['#post'] = $_POST;
  $form['#programmed'] = $form['#redirect'] = FALSE;

  // check if friends checked
  $friends = $form_state['post']['friends'];
  $qty = count($friends);

  // remove the friends
  $count_removed = 0;
  if ($qty > 0) {
    foreach ($friends as $friend_id) {
      $deleted = slcontact_xt_remove_friend($friend_id, $form_state['post']['slcontact_xt_id']);
      drupal_set_message(t('User "!user" removed.', array('!user' =>$deleted->user_name)));
    }
  }
  else {
    drupal_set_message(t('No friends selected.'));
  }
  drupal_set_message(($count_removed <= 1) ? t('!qty Friend removed.', array('!qty' =>$qty)) : t('!qty Friends removed.', array('!qty' =>$qty)));

  // rebuild the form
  drupal_process_form($form_id, $form, $form_state);
  $form = drupal_rebuild_form($form_id, $form_state, $args, $form_build_id);
  $choice_form = $form['slcontact_xt_friendslist']['container'];
  unset($choice_form['#prefix'], $choice_form['#suffix']);
  //drupal_set_message(print_r($choice_form, true));
  $output = theme('status_messages') . drupal_render($choice_form);
  drupal_json(array('status' => TRUE, 'data' => $output));
}